package org.minbox.chapter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动类入口
 */
@SpringBootApplication
public class SpringBootBasicExternalizedConfigurationApplication implements CommandLineRunner {
    /**
     * 注入配置类{@link LoadConfig}
     */
    @Autowired
    private LoadConfig loadConfig;

    public static void main(String[] args) {
        SpringApplication.run(SpringBootBasicExternalizedConfigurationApplication.class, args);
    }


    @Override
    public void run(String... args) throws Exception {
        System.out.println("java system config name value：" + System.getProperty("name"));
        System.out.println("name config value：" + loadConfig.getName());
    }
}
